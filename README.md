### SQS-SES

Aplicação responsável por processar pedidos em uma fila na AWS SQS e disparar um e-mail para o cliente vinculado ao pedido.

Caso haja alguma falha no processamento no pedido, essa aplicação altera o status do mesmo para FAILURE.

Além disso, este serviço, que funciona em background, se autentica na API de administradores para acessar informações no serviço de usuários e no serviço de pedidos.
