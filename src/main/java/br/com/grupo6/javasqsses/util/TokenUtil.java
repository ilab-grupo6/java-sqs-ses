package br.com.grupo6.javasqsses.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Date;

import static br.com.grupo6.javasqsses.util.StaticsUtil.*;

@Log4j2
public class TokenUtil {

    public static String requestToken() throws IOException, InterruptedException {

        log.info("requisitando token");

        log.info(ADMIN_LOGIN_URI);

        Token token = restTemplate.postForObject(ADMIN_LOGIN_URI, login, Token.class);

        log.info(token);

        return token.getToken().replace("Bearer ", "");

    }

    public static boolean isExpirationValid(String token) {
       try {
           if (token == null) return false;
           Jws<Claims> jwsClaims = jwsClaims = Jwts.parserBuilder().setSigningKey(SECRET_KEY.getBytes())
                   .build()
                   .parseClaimsJws(token);
           Date expiration = jwsClaims.getBody().getExpiration();

           return expiration.after(new Date(System.currentTimeMillis()));

       }catch (ExpiredJwtException ex) {
           ex.printStackTrace();
           return false;
       }
    }
}
