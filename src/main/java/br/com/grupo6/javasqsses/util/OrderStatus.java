package br.com.grupo6.javasqsses.util;

public enum OrderStatus {
    PENDING,
    CONCLUDED,
    FAILURE
}
