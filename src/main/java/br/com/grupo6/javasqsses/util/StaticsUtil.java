package br.com.grupo6.javasqsses.util;

import br.com.grupo6.javasqsses.models.Login;
import com.google.gson.Gson;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StaticsUtil {

    public static final long SEGUNDO = 1000;
    public static final long MINUTO = SEGUNDO * 60;
    private final long HORA = MINUTO * 60;

    public static Gson gson = new Gson();
    public static String SECRET_KEY = "3c0MMerc3Do1f00dP@r@T3st3sD3JWT*";
    public static String ADMIN_LOGIN_URI = System.getenv("ADMIN_LOGIN_URI");
    public static String STATUS_ORDER_URI = System.getenv("STATUS_ORDER_URI");
    private static final String username = System.getenv("USERNAME_JAVA_SQS_SES");
    private static final String password = System.getenv("PASSWORD_JAVA_SQS_SES");
    public static RestTemplate restTemplate = new RestTemplate();
    public static HttpHeaders headers = new HttpHeaders();
    public static Login login = new Login(username, password);
    public static Token token= new Token();

}
