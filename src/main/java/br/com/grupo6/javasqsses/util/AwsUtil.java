package br.com.grupo6.javasqsses.util;

import org.springframework.stereotype.Component;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ses.SesClient;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.*;

import java.util.List;

@Component
public class AwsUtil {

    public static List<Message> receiverMessages(SqsClient sqsClient, String queueUrl) {
        ReceiveMessageRequest receiveMessageRequest = ReceiveMessageRequest.builder()
                .queueUrl(queueUrl)
                .waitTimeSeconds(20)
                .maxNumberOfMessages(10)
                .build();
        return sqsClient.receiveMessage(receiveMessageRequest).messages();
    }

    public static GetQueueUrlResponse getQueueUrlResponse(String queueName, String queueOwnerAWSAccountId, SqsClient sqsClient) {
        GetQueueUrlRequest request = GetQueueUrlRequest.builder()
                .queueName(queueName)
                .queueOwnerAWSAccountId(queueOwnerAWSAccountId).build();


        return sqsClient.getQueueUrl(request);
    }

    public static void sendMessage(SqsClient sqsClient, String queueUrl, String message) {
        SendMessageRequest sendMsgRequest = SendMessageRequest.builder()
                .queueUrl(queueUrl)
                .messageBody(message)
                .build();
        sqsClient.sendMessage(sendMsgRequest);
    }

    public static void deleteMenssage(SqsClient sqsClient, String queueUrl, Message message) {

        DeleteMessageRequest deleteMessageRequest = DeleteMessageRequest.builder()
                .queueUrl(queueUrl)
                .receiptHandle(message.receiptHandle())
                .build();
        sqsClient.deleteMessage(deleteMessageRequest);

    }

    public static final AwsCredentialsProvider credentialsProvider = new AwsCredentialsProvider() {
        @Override
        public AwsCredentials resolveCredentials() {
            return new AwsCredentials() {
                @Override
                public String accessKeyId() {
                    return awsAccessKey;
                }

                @Override
                public String secretAccessKey() {
                    return awsSecretKey;
                }
            };
        }
    };



    public static SqsClient sqsClient = SqsClient.builder()
            .region(Region.US_EAST_1)
            .credentialsProvider(credentialsProvider)
            .build();

    public static SesClient sesClient = SesClient.builder()
            .region(Region.US_EAST_1)
            .credentialsProvider(credentialsProvider)
            .build();

    public static String awsAccessKey = System.getenv("AWS_ACCESS_KEY");
    public static String awsSecretKey = System.getenv("AWS_SECRET_KEY");
    public static String awsAccountId = System.getenv("AWS_ACCOUNT_ID");
    public static String queue = System.getenv("AWS_QUEUE");
}
