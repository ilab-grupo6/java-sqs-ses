package br.com.grupo6.javasqsses;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import static br.com.grupo6.javasqsses.services.SqsService.processOrder;
import static br.com.grupo6.javasqsses.util.AwsUtil.*;
import static br.com.grupo6.javasqsses.util.StaticsUtil.SEGUNDO;

@Component
@EnableScheduling
public class App {

    @Scheduled(fixedDelay = 25 * SEGUNDO)
    public static void main() {
        try {
            processOrder(sqsClient, queue, awsAccountId);
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
