package br.com.grupo6.javasqsses.models;

import br.com.grupo6.javasqsses.util.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private UUID id;
    private UUID userId;
    private Timestamp orderDate;
    private OrderStatus status;
    private Long value;
    private  String description;
}
