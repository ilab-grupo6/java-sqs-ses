package br.com.grupo6.javasqsses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSqsSesApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSqsSesApplication.class, args);
	}

}
