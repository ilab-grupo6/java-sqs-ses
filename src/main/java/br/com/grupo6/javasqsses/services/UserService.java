package br.com.grupo6.javasqsses.services;

import br.com.grupo6.javasqsses.exceptions.UserNotFoundException;
import br.com.grupo6.javasqsses.models.User;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.UUID;

import static br.com.grupo6.javasqsses.util.StaticsUtil.gson;
import static br.com.grupo6.javasqsses.util.StaticsUtil.token;
import static br.com.grupo6.javasqsses.util.TokenUtil.isExpirationValid;
import static br.com.grupo6.javasqsses.util.TokenUtil.requestToken;

@Service
public class UserService {

    public static User findUserById(UUID id) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        if (token.getToken() == null || !isExpirationValid( token.getToken())) {
            token.setToken(requestToken());
        }
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .timeout(Duration.ofSeconds(10))
                .uri(URI.create(System.getenv("USER_BY_ID_URI")+id))
                .header("Authorization", token.getToken())
                .build();

        HttpResponse<String> httpResponse = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        if (httpResponse.statusCode() != 200) throw new UserNotFoundException("User not found");

        return gson.fromJson(httpResponse.body(), User.class);

    }
}
