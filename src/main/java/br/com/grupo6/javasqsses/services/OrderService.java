package br.com.grupo6.javasqsses.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.UUID;

import static br.com.grupo6.javasqsses.util.StaticsUtil.*;
import static br.com.grupo6.javasqsses.util.StaticsUtil.token;
import static br.com.grupo6.javasqsses.util.TokenUtil.isExpirationValid;
import static br.com.grupo6.javasqsses.util.TokenUtil.requestToken;

@Service
@Log4j2
public class OrderService {
    public static void closeOrder(UUID id) throws IOException, InterruptedException {
        if (token.getToken() == null || !isExpirationValid( token.getToken())) {
            token.setToken(requestToken());
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+ token.getToken());

        HttpEntity<Void> entity = new HttpEntity<Void>(null,headers);
        String uri = STATUS_ORDER_URI+id+"?status=CONCLUDED";
        log.info(uri);
        restTemplate.put(uri, entity, Void.class);
    }

    public static void failOrder(UUID id) throws IOException, InterruptedException {
        if (token.getToken() == null || !isExpirationValid( token.getToken())) {
            token.setToken(requestToken());
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+ token.getToken());

        HttpEntity<Void> entity = new HttpEntity<Void>(null,headers);
        String uri = STATUS_ORDER_URI+id+"?status=FAILURE";
        log.info(uri);
        restTemplate.put(uri, entity, Void.class);
    }
}
