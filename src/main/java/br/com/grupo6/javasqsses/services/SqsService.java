package br.com.grupo6.javasqsses.services;

import br.com.grupo6.javasqsses.models.Order;
import br.com.grupo6.javasqsses.models.User;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.GetQueueUrlResponse;
import software.amazon.awssdk.services.sqs.model.Message;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static br.com.grupo6.javasqsses.services.OrderService.closeOrder;
import static br.com.grupo6.javasqsses.services.OrderService.failOrder;
import static br.com.grupo6.javasqsses.services.SesService.sendEmail;
import static br.com.grupo6.javasqsses.services.UserService.findUserById;
import static br.com.grupo6.javasqsses.util.AwsUtil.*;
import static br.com.grupo6.javasqsses.util.StaticsUtil.gson;

@Log4j2
@Service
public class SqsService {


    public static void processOrder(SqsClient sqsClient, String queue, String awsAccountId) {

        GetQueueUrlResponse result = getQueueUrlResponse(queue, awsAccountId, sqsClient);

        List<Message> messages =  receiverMessages(sqsClient, result.queueUrl());

        //String bodyText = "Olá este é um email";


       if (messages.size() > 0) {

           messages.forEach(msg -> {
               try {

                   Order order = gson.fromJson(msg.body(), Order.class);
                   failOrder(order.getId());
                   User user = findUserById(order.getUserId());
                   Locale localBrasil = new Locale("pt", "BR");
                   SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", localBrasil);
                   GregorianCalendar gc = new GregorianCalendar(localBrasil);
                   gc.setTimeInMillis(order.getOrderDate().getTime());
                   gc.add(Calendar.HOUR_OF_DAY, -3);

                   String bodyHTML = "<html>"
                           + "<head></head>"
                           + "<body>"
                           + "<h1>Olá, "+ user.getName() +"! Recebemos o seu novo pedido :)</h1>"
                           + "<ul>"
                           + "<li>ID do pedido: "+order.getId()+"</li>"
                           + "<li>CPF do(a) cliente: "+user.getCpf()+"</li>"
                           + "<li>Data do pedido: "+format.format(gc.getTime())+"</li>"
                           + "<li>Status do pedido: FINALIZADO</li>"
                           + "<li>Valor do pedido: "+ NumberFormat.getCurrencyInstance(localBrasil).format(order.getValue()/100) +"</li>"
                           + "</ul>"
                           + "<p>Descrição do pedido: "+order.getDescription()+"</p>"
                           + "</body>"
                           + "</html>";

                   boolean sent = sendEmail(sesClient, "noreply@groupsixilab.tk",
                           user.getEmail(),
                           "Recebemos o seu pedido","" ,bodyHTML);

                   if(sent) {
                       closeOrder(order.getId());
                       deleteMenssage(sqsClient, queue, msg);
                   }

               } catch (Exception e) {
                   log.info(e.getMessage());
                   log.info(e.getCause().getMessage());
                   e.printStackTrace();
               }

           });
       }

    }
}
